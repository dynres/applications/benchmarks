# DPP Benchmarks

This repo provides an environment for generating and running benchmarks with the DPP software stack

## Table of Contents
- [1. Prerequisites](#prerequisites)
    - [1.1. Docker Cluster](#docker-cluster)
    - [1.2. Nix-OS-compose](#nix-os-compose)
- [2. Usage](#usage)
    - [2.1. Generating Experiments](#generating-experiments)
    - [2.2. Running Experiments](#running-experiments)
- [3. Adding Experiments](#adding-experiments)
    - [3.1. Adding Applications](#adding-applications)
    - [3.2. Adding Experiment Collections](#adding-experiment-collections)

## Prerequisites
### [Docker-Cluster](https://gitlab.inria.fr/dynres/dyn-procs/docker-cluster):
Follow the instructions in the docker-cluster README.
Inside the docker cluster use the [dyn_procs_setup repo](https://gitlab.inria.fr/dynres/dyn-procs/dyn_procs_setup/-/tree/docker_setup?ref_type=heads):

```. envs.sh benchmarks --install --load --export --show```

### Nix-OS-compose:
    ...

## Usage
### Generating Experiments
To generate an instance of an experiment, run:
```
python3 generate_experiments.py collection_name experiment name --num_nodes=[num_nodes] --cores_per_node=[cores_per_node] --ram_per_node=[ram_per_node] [--overwrite] [--verbosity=verbosity]

    collection_name     The name of the collection the experiment definition is in
                        I.e. experiments/collection_name/collection_name.py
    experiment_name     The name of the experiment to generate. 
                        I.e. the default value of name argument of the generator function
                        Use 'all' to generate all experiments of the given collection
                        Use '*' suffix or prefix to match multiple instances
    --num_nodes         The number of nodes to run the experiment on
    --cores_per_node    The number of cores to be use on each node
    --ram_per_node      The amount of RAM available per node (GB)
    --overwrite         Force overwritting of existing generated experiments
    --verbosity         Level of verbosity: 0-3, default 0
```
This will create a tarball in the achive directory. The naming scheme of the tarball is:
```
[eperiment_name]_n[num_nodes]_c[cores_per_node].tar.gz
```

Example:
```
python3 generate_experiments.py example exp_all_examples --num_nodes=8 --cores_per_node=4 --ram_per_node=1
```
==> Generates a compound experiment called 'exp_all_examples' from the 'example' collection, consisting of two sub experiments for a 8 node sytsem where each node has 4 cores and 1 GB RAM

### Running Experiments
To run an instance of an experiment, run:
```
python3 run_experiments.py collection_name instance_name --target=[execution target] [--verbosity=verbosity]

    collection_name     The name of the collection the experiment definition is in
                        i.e. experiments/collection_name/collection_name.py
    instance_name       The name of the experiment instance. 
                        i.e. the name of the tarball (without .tar.gz)
                        Use 'all' to run all experiment instances of the given collection
                        Use '*' suffix or prefix to match multiple instances
    --target            Target environment the experiment intance is executed in:
                        {'bare-metal', 'slurm-env',  ...}
    --verbosity         Level of verbosity: 0-3, default 0
```
This will create a directory in the 'runs' directory of the tarball conatining the output. 
The naming scheme of this directory is:
```
[eperiment_name]_n[num_nodes]_c[cores_per_node]_[target]_[date_time]
```

Example:
```
python3 run_experiments.py  example exp_all_examples* --target=bare-metal 
```
==> Runs a compound experiment called 'exp_all_examples' from the 'example' collection, consisting of two sub experiments on a bare-metal system

## Adding Experiments
The repository can easily be extended with your own experiments
### Adding Applications
If your experiment requires running applications other than the example application it can be added to the repo:

- Optional: Add your application source code
    - Add your application source code as submodule in `applications/src`. 
    - Update the Makefile in `applications/Makefile` to build your application (there are hints included in the Makefile) 
    - Note: Alternatively, it is suffient to make the application executable available in the `PATH`.
- Add an application executable template under `applications/template`. 
    - This is a template for a bash script which will call your application executable
    - See `applications/template/dyn_psets_template.txt` for reference
    - Run command should be in the form:
        ```
        executable_name \
            --timestamps $BENCHMARKS_ROOT/{timestamp_file} \ # filename for writing timestamps data
            --inhibitor {inhibitor} \ # inhibitor for resorce changes
            --iterations {iterations} \ # number of iterations
            --monitoring {enable_monitoring} \ # {0,1} Wether monitoring should be enabled
            --[app_specific_parameter] {app_specific_parameter}
            >> $BENCHMARKS_ROOT/{output_file} 2>&1 # File for writing stdout and stderr
        ```
    - If your app does not support all common arguments (such as inhibitor) the template shall write this line to the output file before starting the application: 
        ```
        echo "Parameter --parameter not supported. Value: {$paramter}" >> $BENCHMARKS_ROOT/{output_file} 2>&1
        ``` 
### Adding Experiment Collections
Multiple Experiments are usually organized in a collection:
- Create a directory called `[your_collection_name]` under `experiments`
- Create a directory called `templates` under `experiments/[your_collection_name]`
    - Add experiment specific app template dirs under `experiments/[your_collection_name]/templates`:
        - Example: The `app_mix1` dir will contain templates for applications used in a job mix
    - Add application templates in the experiment specific app template dir:
        - Example: `app_mix1/app_1_template.py` contains default parameters for app1 in app_mix1 
        - Template Structure (also see templates in `experiments/examples/templates/example_mix` for reference):
        ```
        EXECUTABLE_TEMPLATE = "dyn_psets_template.txt"

        # EXECUTABLE ARGUMENTS (References the arguments in the executable template)
        EXECUTABLE_ARGUMENTS = {
            'param1' : 'value1',
            'param2' : value2
        }

        # SUBMISSION ARGUMENTS (references parameters of batch script see 'submission_template' in utils/workloads/generate_submissions)
        SUBMISSION_ARGUMENTS = {
            "model" : "AmdahlPsetModel",
            ...,
            ...
        }
        ```
- Create a file called `[your_collection_name].py` under `experiments/[your_collection_name]`
    - This file will contain the experiment definitions. Currently there are 3 types:
        - Compound: Runs multiple sub experiments
        - Single App: Runs a single application
        - Job Mix: Runs a job mix
    - See `experiments/examples/examples.py` for details on creating experiments of different types
    - Experiments will overwrite certain paramters from the executable & submission templates 
- You can now generate and run your experiment as shown in Section [Usage](#usage)


## Contact:
domi.huber@tum.de