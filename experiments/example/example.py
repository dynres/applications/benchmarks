from utils.workloads.generate_submission import *
from utils.workloads.generate_job_mix import *
from utils.metadata.metadata_generation import *


# Required Paramters for your generation functions:
# name              => Name of experiment. THE DEFAULT VALUE IS USED BY GENERATE_EXPERIENTS SCRIPT!
# num_nodes         => Number of nodes available in the system
# cores_per_node    => Cores available per node
# ram_per_node      => RAM available per node



#########################################################
#                                                       #
#                                                       #
#     Counpound Experiment:                             #
#       => Generates Experiment 1 & 2                   #
#                                                       #
#                                                       #
#########################################################
# Generates a compound experiment consisting of sub experiments
def generate_exp_all_examples(name='exp_all_examples', num_nodes=8, cores_per_node=4, ram_per_node=1, overwrite=False, verbosity=0):
    timeout = -1
    return generate_compound(name, num_nodes, cores_per_node, ram_per_node, 
                            overwrite, # Overwrite existing experiments with this name?
                            timeout, # Timeout for running the experiment
                            verbosity, # Verbosity of generation process
                            ['plot_name'], ['plot_params'], # Plot_script and params for this experiment 
                            'example',                         # Experiment Collection for subexperiments
                            ['exp_01', 'exp_02'])           # Sub experiments to generate

#########################################################
#                                                       #
#                                                       #
#     Experiment 1: Single App Extensions               #
#                                                       #
#                                                       #
#########################################################
exp_01_t_s = 3 # Number of seconds for sequential part per iteration
exp_01_t_p = 1 # Number of seconds for parallel part per iteration
exp_01_inhibitor = 2 # Send/Check SetOp every <<inhibitor>> iterations

# 
def generate_exp_01(
        name='exp_01', num_nodes=8, cores_per_node=4, ram_per_node=1, overwrite=False, verbosity=0):

    iterations = (num_nodes - 1) * exp_01_inhibitor
    timeout = iterations * (exp_01_t_s + exp_01_t_p) * 2

    # Executable Paramters to overwrite 
    executable_params = {'inhibitor' : exp_01_inhibitor,
                         'iterations' : iterations,
                         'blocking' : False}
    # Submission Paramters to overwrite
    submission_params = {'num_delta' : cores_per_node, 
                         'num_delta_add' : cores_per_node}

    # Generate the single_app_run experiment
    return generate_single_app_run(name, num_nodes, cores_per_node, ram_per_node, overwrite, timeout, verbosity,
                            "Discrete_Steepest_Ascend", {"step_size" : "linear"}, # Scheduling Policy and paramters
                            ['plot_name'], ['plot_params'], # Plot scripts and params
                            'example', 'example_mix', 'dyn_psets_good_scalability', # experiment template to use
                            executable_params, submission_params) # executable and submission params to overwrite in template


#########################################################
#                                                       #
#                                                       #
#     Experiment 2: Multi App Scheduling                #
#                                                       #
#                                                       #
#########################################################
exp_02_lambda = 3 # Lambda parameter for Poisson Distribution of arrival times
exp_02_efficiency = 0.9     # Parallel efficiency of static execution
exp_02_t_iter = 10          # Maximum time per iteration (worst case: 1 node)
exp_02_max_time = 200       # Maximum runtime of the static jobs
exp_02_num_jobs = 5         # Number of jobs in the job mix


def generate_exp_04_02(name='exp_02', num_nodes=8, cores_per_node=4, ram_per_node=1, num_jobs=None, inter_arrival=None, pe=None, t_iter=None, overwrite=False, verbosity=0):
    timeout = 3600

    return generate_job_mix_run(name, 
                                'example', 'example_mix',      # template dir to use as basis for job mix
                                num_nodes, cores_per_node, ram_per_node, 
                                exp_02_num_jobs,            # Number of jobs in the job mix
                                exp_02_lambda,              # Paramter for interarrival time
                                exp_02_efficiency,          # Paramter for interarrival time
                                max_time=exp_02_max_time,   # Parallel efficiency of static execution
                                enable_monitoring=False,    # Enable/Disable monitoring
                                timeout=timeout,            # Timeout for experiment run
                                policy = 'Discrete_Steepest_Ascend', policy_params={'step_size' : 'power_of_2'}, # Policy and Policy Params
                                plots=['plot_name'], plot_params=['plot_params'], overwrite=overwrite, verbosity=verbosity)

