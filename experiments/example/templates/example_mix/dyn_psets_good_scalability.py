# EXECUTABLE TEMPLATE
EXECUTABLE_TEMPLATE = "dyn_psets_template.txt"

# EXECUTABLE ARGUMENTS
EXECUTABLE_ARGUMENTS = {
    "rm" : 'dyn_rm',
    "blocking" : 0,
    "iterations" : 100,
    "inhibitor" : 1,
    "enable_monitoring" : False,
    "t_s": 2,
    "t_p": 11,
    "generator_key" : "replace_generator"
}

# SUBMISSION ARGUMENTS
SUBMISSION_ARGUMENTS = {
    "model" : "AmdahlPsetModel",
    "t_s" : 2,
    "t_p" : 11, 
    "num_delta" : -1,
    "factor" : -1,
    "mapping" : 'dense',
    "num_delta_add" : -1, 
    "num_delta_sub" : -1, 
    "power_of_two" : False, 
    "num_max"  : 99999999,
    "num_min"  : -1, 
    "runtime"  : 1200, 
    "num_nodes" : 4,
    "verbosity" : 0
}