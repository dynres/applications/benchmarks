import argparse
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker 
from plot_utils import Colors
from load_benchmark_data import load_node_data, load_job_data, load_policy_data

TEXTSIZE = 25



# function to add value labels
def addlabels(ax, x, y, lables, limits):
    for i in range(len(x)):
        pos = y[i] + 0.075
        if pos > limits[1]:
            pos = -10000000000
        if pos < limits[0]:
            pos = 10000000000

        ax.text(x[i], pos, lables[i], ha = 'center', rotation='vertical', fontsize=20, weight='bold')

def plot_bar_group(plot, center, values, bar_labels, val_labels, bar_width=1, limits = (-10000000000, 10000000000)):
    colors = Colors()
    bar_colors = [colors.get_next() for x in range(len(values))]
    
    num_bars = len(values)
    x = [center - (num_bars/2 - index) for index in range(num_bars)]

    plot.bar(x, 
             values, 
             width=bar_width,
             color=bar_colors,
             edgecolor='black',
             linewidth=0.1,  
             label=val_labels,
             zorder = 3)
    print("PLOT BARGROUP")
    if None != bar_labels:
        addlabels(plot, x, values, bar_labels, limits)

    return plot

def get_parameters():
    parser = argparse.ArgumentParser() # Add an argument
    parser.add_argument('--dirs', type=str, required=True)
    parser.add_argument('--labels', type=str, required=True)
    parser.add_argument('--output', type=str, required=True)
    parser.add_argument('--relative', type=bool, required=False, default=True)
    args = parser.parse_args()

    return  args.dirs.split(','),\
            args.labels.split(','),\
            args.output, \
            args.relative

# example: python3 plot_system_metrics.py --dirs input/synthetic_job_mix/static_sparse,input/synthetic_job_mix/dynamic_sparse,input/synthetic_job_mix/dynamic_sparse_monitor,input/synthetic_job_mix/static_dense,input/synthetic_job_mix/dynamic_dense,input/synthetic_job_mix/dynamic_dense_monitor --labels static_sparse,dynamic_sparse,dynamic_sparse_monitor,static_dense,dynamic_dense,dynamic_dense_monitor --output systen_metrics.pdf
if __name__ == "__main__":
    
    dirs, group_labels, output, relative = get_parameters()
    
    #fig = plt.figure(figsize=(16,9))
    f, (ax, ax2) = plt.subplots(2, 1, sharex=True, figsize=(16,9))
    #ax2.axhline(y = 1.0, color = 'gray', linestyle = '--', linewidth=2, zorder=-1)
    #plot = plt.gca()
    plot = ax2


    # zoom-in / limit the view to different portions of the data
    ax.set_ylim(3.01, 4.99)  # outliers only
    ax2.set_ylim(0, 1.9)  # most of the data

    metrics = [
        'makespan', 
        #'avg_system_perf', 
        'avg_pending_time', 
        'avg_run_time', 
        'avg_turnaround_time', 
        'avg_utilization'
        ]
    
    units = [
        's',
        's',
        's',
        's',
        '%'
    ]

    center = 0
    x_tick_pos = []
    first = True
    base_values = None
    for dir, group_label in zip(dirs, group_labels):
        
        node_data = load_node_data(dir)
        job_data = load_job_data(dir)
        policy_data = load_policy_data(dir)

        values = \
            [   job_data['makespan'],
                #policy_data['avg_system_perf'], 
                job_data['avg_queue_time'],
                job_data['avg_run_time'],
                job_data['avg_turnaround_time'],
                node_data['avg_utilization']
                ]
        
        values = [int(round(val, 0)) for val in values]
        if first:
            first = False
            val_labels = metrics
            base_values = [val for val in values]
            print(base_values)
        else:
            val_labels = None
        
        bar_labels = [str(val)+""+str(unit) for val,unit in zip(values, units)]
        
        if relative:
            values = [val/base for val,base in zip(values, base_values)]
            
        
        center += 1.5*len(values)
        x_tick_pos.append(center) 

        plot = plot_bar_group(plot, center, values, bar_labels, val_labels, limits=(0,2))
        ax = plot_bar_group(ax, center, values, bar_labels, val_labels, limits=(3,5))
    
    ax.tick_params(labelsize=TEXTSIZE+5)
    ax2.tick_params(labelsize=TEXTSIZE+5)
    #plt.ylim(0,4)
    f.supylabel('Relative Metric Value', x=0.035, fontsize=TEXTSIZE+5)

    plt.xticks(ticks=x_tick_pos, labels=group_labels, fontsize=TEXTSIZE+5)
    ax.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax.yaxis.set_major_locator(ticker.MultipleLocator(0.5))
    ax2.yaxis.set_major_locator(ticker.MultipleLocator(0.5)) 
    
    d = .015  # how big to make the diagonal lines in axes coordinates
    # arguments to pass to plot, just so we don't keep repeating them
    kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
    ax.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
    ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

    kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
    ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
    ax2.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal
    
    plt.hlines(y = 1.0, color = 'gray', xmin=(1.5*len(metrics) - len(metrics)/2) - 0.5, xmax=center + (len(metrics)/2) -0.5, linestyle = '--', linewidth=2, zorder=-1)
    
    ax.legend(fontsize=TEXTSIZE)

    plt.savefig('figures/'+output)
    plt.show()
