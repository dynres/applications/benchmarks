

class Colors:
    colors = ['blue', 'green', 'black', 'gray', 'orange', 'purple', 'red']
    def __init__(self):
        self.index = 0
    
    def get_next(self):
        color = Colors.colors[self.index]
        self.index = (self.index + 1) % len(Colors.colors)
        return color

class Markers:
    markers = ['o', 'v', '1', '*', 's', 'D', 'x']
    def __init__(self):
        self.index = 0
    
    def get_next(self):
        marker = Markers.markers[self.index]
        self.index = (self.index + 1) % len(Markers.markers)
        return marker
