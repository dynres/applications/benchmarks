import argparse
import matplotlib.pyplot as plt
from numpy import median

from plot_utils import Colors
from load_benchmark_data import load_iter_data

def plot_bar_group(plot, center, values, group_label, val_labels, bar_width=1):
    colors = Colors()
    bar_colors = [colors.get_next() for x in range(len(values))]
    
    num_bars = len(values)
    x = [center - (num_bars/2 - index) for index in range(num_bars)]
    print(x)
    print(val_labels)
    plot.bar(x, 
             values, 
             width=bar_width,
             color=bar_colors,
             edgecolor='black',
             linewidth=0.1,  
             label=val_labels)

    return plot

def get_parameters():
    parser = argparse.ArgumentParser() # Add an argument
    parser.add_argument('--input', type=str, required=True)
    parser.add_argument('--output', type=str, required=True)
    parser.add_argument('--factors', type=str, required=False)
    parser.add_argument('--labels', type=str, required=False)
    args = parser.parse_args()
    factors = None
    if args.factors!= None:
        factors = [int(x) for x in args.factors.split(',')]
    else:
        factors = [1 for x in args.input.split(',')]
    if args.labels== None:
        labels = args.input.split(',')
    else:
        labels = args.labels.split(',')
    return  args.input,\
            args.output, \
            factors, \
            labels

# example: python3 plot_system_metrics.py --dirs input/synthetic_job_mix/static_sparse,input/synthetic_job_mix/dynamic_sparse,input/synthetic_job_mix/dynamic_sparse_monitor,input/synthetic_job_mix/static_dense,input/synthetic_job_mix/dynamic_dense,input/synthetic_job_mix/dynamic_dense_monitor --labels static_sparse,dynamic_sparse,dynamic_sparse_monitor,static_dense,dynamic_dense,dynamic_dense_monitor --output systen_metrics.pdf
if __name__ == "__main__":
    
    input, output, factors, labels = get_parameters()
    colors = Colors()
    
    fig, axs = plt.subplots(3, sharex=True)
    fig.subplots_adjust(wspace=None, hspace=0.4)
    #fig.tight_layout()
    #axs[0].set_ylabel('Time (Sec)', fontsize=10)
    #axs[0].set_xlabel('Number of processes', fontsize=10)
    axs[0].tick_params(axis='both', which='major', labelsize=10)

    axs[1].set_ylabel('Time (MSec)', fontsize=10, fontweight='bold')
    #axs[1].set_xlabel('Number of processes', fontsize=10)
    axs[1].tick_params(axis='both', which='major', labelsize=10)

    #axs[2].set_ylabel('Time (Msec)', fontsize=10)
    axs[2].set_xlabel('Number of processes', fontsize=10, fontweight='bold')
    axs[2].tick_params(axis='both', which='major', labelsize=10)

    files = input.split(',')
    num_files = len(files) 

    i = 0
    for file in files:
        iter_data = load_iter_data(file)

        phases = dict()
        for iter in iter_data.values():
            if 'adapt_start_procs' not in iter:
                continue
            num_procs = iter['adapt_start_procs']
            if num_procs in phases:
                phase = phases[num_procs]
            else:
                phase = dict()
                phase['work_duration'] = [0]
                phase['num_iters'] = 0
                phase['adapt_overhead'] = 0
                phase['redist_overhead'] = 0
                phase['num_procs'] = num_procs
                phases[num_procs] = phase

            phase['work_duration'].append(iter['work_duration'])
            phase['num_iters'] += 1
            if iter['adapt_start_procs'] != iter['adapt_end_procs']:
                phase['adapt_overhead'] = iter['adapt_overhead'] - iter['redist_duration']
                phase['redist_overhead'] = iter['redist_duration']

        for phase in phases.values():
            phase['work_duration'] = median(phase['work_duration'])
        phases = {k:v for k,v in sorted(phases.items(), key=lambda x: x[1]['num_procs'])}

        x_pos = [(x*(num_files+1))+i for x in range(len(phases.keys()))]
        print(x_pos)
        work_durations = [factors[i]*x['work_duration']/1000 for x in phases.values()]
        adapt_overheads= [x['adapt_overhead']/1000 for x in phases.values()]
        redist_overheads= [x['redist_overhead']/1000 for x in phases.values()]
        print(work_durations)
        print(adapt_overheads)
        print(redist_overheads)
        color = colors.get_next()
        if i == 0:
            tick_pos =[(x*(num_files+1)) +float(num_files)/2.0 for x in range(len(phases.keys()))]
            axs[0].set_xticks(ticks=tick_pos, labels=[str(x) for x in phases.keys()])
            axs[2].set_xticks(ticks=tick_pos, labels=[str(x) for x in phases.keys()])
            axs[1].set_xticks(ticks=tick_pos, labels=[str(x) for x in phases.keys()])

        
        axs[0].bar(x_pos, work_durations, width=1, color=color, label=labels[i])
        axs[2].bar(x_pos, adapt_overheads, width=1, color=color, label=labels[i])
        axs[1].bar(x_pos, redist_overheads, width=1, color=color, label=labels[i])
            
        i+=1
    #plot.bar([x for x in iter_data.keys()][:300], [x["work_duration"] for x in iter_data.values()][:300])
    #plot.bar([x for x in iter_data.keys()], [x["adapt_overhead"]/1000000 for x in iter_data.values() if "adapt_overhead" in x])

    #print(iter_data.keys())
    axs[0].ticklabel_format(style='scientific', axis='y', scilimits=(0, 0))
    axs[1].ticklabel_format(style='scientific', axis='y', scilimits=(0, 0))
    axs[2].ticklabel_format(style='scientific', axis='y', scilimits=(0, 0))
    
    axs[0].legend()
    axs[1].legend()
    axs[2].legend()

    axs[0].set_title('Workload Duration')
    axs[1].set_title('Data Redistribution Overhead')
    axs[2].set_title('MPI Reconfiguration Overhead')

    axs[1].set_yscale('log')

    
    plt.show()
    fig.savefig('figures/'+output)