import matplotlib.pyplot as plt
import csv
from collections import OrderedDict
import json
from os.path import join,exists

def load_node_data(dir):

    start_time = 0
    frame_list = list()
    frames = dict()
    data = dict()
    
    num_nodes = 0
    # timestamp, node_id, event, num_cores, num_free_cores, num_utilized_cores
    with open(join(dir, "node.csv"), newline='') as csvfile:
        csvreader = csv.reader(csvfile)
        header = next(csvreader)
        def _(key, lookup_dict=header):
            return lookup_dict.index(key)
        
        frame0 = dict()
        frame0['event'] = 'system_start'
        frame0['time'] = 0
        frame0['utilization'] = 0
        frame0['nodes'] = dict()
        for row in csvreader:
            node_id = row[_('node_id')]
            if node_id not in frame0['nodes'].keys():
                num_nodes += 1
                frame0['nodes'][node_id] = {'status': 'down', 
                                       'num_cores' : int(row[_('num_cores')]),
                                       'num_free_cores' : int(row[_('num_cores')]),
                                       'num_utilized_cores' : 0}
        frames[0] = frame0
        frame_list.append(frame0)

    with open(join(dir, "node.csv"), newline='') as csvfile:
        csvreader = csv.reader(csvfile)
        header = next(csvreader)
        def _(key, lookup_dict=header):
            return lookup_dict.index(key)
        for row in csvreader:
            frame = frame_list[-1].copy()
            timestamp = float(row[_('timestamp')])
            
            if start_time == 0:
                start_time = timestamp

            frame['event'] = row[_('event')]
            frame['time'] = timestamp - start_time
            frame['nodes'][row[_('node_id')]]['num_cores'] = int(row[_('num_cores')])
            frame['nodes'][row[_('node_id')]]['num_free_cores'] = int(row[_('num_free_cores')])
            frame['nodes'][row[_('node_id')]]['num_utilized_cores']= int(row[_('num_utilized_cores')])
            frame['utilization'] = len([node for node in frame['nodes'].values() if node['num_utilized_cores'] > 0])/len([node for node in frame['nodes'].values()])*100
            if['event'] == "NODE_STARTED":
                frame['nodes'][row[_('node_id')]]['status'] = 'up'

            frames[timestamp - start_time] = frame
            frame_list.append(frame)
        
    avg_util = 0
    total_time = frame_list[-1]['time']
    index = 0
    for index in range(len(frame_list) -1):
        frame_list[index]['interval_len'] = (frame_list[index + 1]['time'] - frame_list[index]['time'])
        avg_util += frame_list[index]['utilization']*(frame_list[index]['interval_len'])
        
    avg_util /= total_time

    data['total_interval'] = total_time
    data['num_nodes'] = num_nodes
    data['avg_utilization'] = avg_util
    data['frames'] = frames
    data['frame_list'] = frame_list
    return data

def load_job_data(dir, filename="job.csv"):

    start_time = 0
    data = dict()
    data['jobs'] = dict()
    
    num_nodes = 0
    # timestamp,event,task_id,task_name,task_graph_id
    with open(join(dir, filename), newline='') as csvfile:
        csvreader = csv.reader(csvfile)
        header = next(csvreader)

        def _(key, lookup_dict=header):
            return lookup_dict.index(key)
        
        for row in csvreader:
            job_id = row[_('task_id')]
            graph_id = row[_('task_graph_id')]
            if graph_id == "":
                continue
            if job_id not in data['jobs']:
                data['jobs'][job_id] = dict()
                data['jobs'][job_id]['name'] = row[_('task_name')]

            
            job = data['jobs'][job_id]
            event = row[_('event')]
            timestamp = float(row[_('timestamp')])
            if event == "TASK_SUBMITTED":
                print("submitted "+job_id)
                job["submitted"] = timestamp
            elif event == "TASK_STARTED":
                job["started"] = timestamp
                job['queue_time'] = job["started"] - job["submitted"]
                job["terminated"] = timestamp
                job['run_time'] = job["terminated"] - job["started"]
                job['turnaround_time'] = job["terminated"] - job["submitted"]            
            elif event == "TASK_TERMINATED":
                job["terminated"] = timestamp
                job['run_time'] = job["terminated"] - job["started"]
                job['turnaround_time'] = job["terminated"] - job["submitted"]
    
    jobs = data['jobs'].values()        
    start_time = min([job["submitted"] for job in jobs])
    end_time = max([job["terminated"] for job in jobs if "terminated" in job])
    print([job["submitted"] for job in jobs])
    print("Start time: ", start_time)
    print("End time: ", end_time)
    data['makespan'] = end_time - start_time
    data['avg_queue_time'] = sum([job['queue_time'] for job in jobs])/len(jobs)
    data['avg_run_time'] = sum([job['run_time'] for job in jobs])/len(jobs)
    data['avg_turnaround_time'] = sum([job['turnaround_time'] for job in jobs])/len(jobs)
    data['num_jobs'] = len(jobs)

    return data

def load_setop_data(dir):


    data = dict()
    data['jobs'] = dict()

    #timestamp,event,id,alias,op,status,input,output,nodelist_in,nodelist_out,predecessors,successors
    with open(join(dir, "setop.csv"), newline='') as csvfile:
        csvreader = csv.reader(csvfile)
        header = next(csvreader)
        print(header)
        def _(key, lookup_dict=header):
            return lookup_dict.index(key)
        
        for row in csvreader:
            
            setop_id = row[_('id')]
            if setop_id == "MCAGraphObject:/":
                continue

            # Ignore cancelation
            op = int(row[_('op')])
            if op == 11:
                continue

            job_id = "MCAGraphObject://0/"+ str(int(setop_id.split("MCAGraphObject://0/")[1].split("/")[0])-1)+"/"+"1"

            if job_id not in data['jobs']:
                data['jobs'][job_id] = dict()
                data['jobs'][job_id]['setops'] = dict()
            
            setops = data['jobs'][job_id]['setops']
            if setop_id not in setops:
                data['jobs'][job_id]['setops'][setop_id] = dict()
            
            setop = data['jobs'][job_id]['setops'][setop_id]
            event = row[_('event')]
            timestamp = float(row[_('timestamp')])

            if event == "SETOP_DEFINED":
                setop['op'] = op
                setop['defined'] = timestamp
                setop['input'] = row[_('input')].split(';')
                setop['nodes_before'] = row[_('nodelist_in')]
                setop['num_nodes_before'] = 0 if setop['nodes_before'] =='' else len(row[_('nodelist_in')].split(';'))
            elif event == "SETOP_SCHEDULED":
                setop['scheduled'] = timestamp
                setop['unscheduled_time'] = timestamp - setop['defined']
                setop['output'] = row[_('output')].split(';')
                setop['predecessors'] = [] if "" == row[_('predecessors')] else row[_('predecessors')].split(';')
                setop['successor'] = [] if "" == row[_('successors')] else row[_('successors')].split(';')
                setop['nodes_after'] = row[_('nodelist_out')]
                setop['num_nodes_after'] = 0 if setop['nodes_after'] =='' else len(row[_('nodelist_out')].split(';'))
                print(setop['nodes_after'].split(';'))
                print(setop['num_nodes_before']," -> ", setop['num_nodes_after'])
                setop['num_nodes_delta'] = setop['num_nodes_after'] - setop['num_nodes_before']
                print(setop['num_nodes_delta'])
            elif event == "SETOP_STARTED":
                setop['started'] = timestamp
                setop['scheduled_time'] = timestamp - setop['started']
            elif event == "SETOP_FINALIZED":
                setop['finalized'] = timestamp
                if 'started' in setop:
                    setop['execution_time'] = timestamp - setop['started']
                else:
                    setop['execution_time'] = timestamp - setop['scheduled'] 
        
    for job_id in data['jobs'].keys():
       data['jobs'][job_id]['num_setops'] = len(data['jobs'][job_id]['setops'])
    
    return data


def load_set_data(dir):
    
    data = dict()
    data['jobs'] = dict()

    #timestamp,event,id,task_id,size,model,model_params    
    with open(join(dir, "set.csv"), newline='') as csvfile:
        csvreader = csv.reader(csvfile)
        header = next(csvreader)

        def _(key, lookup_dict=header):
            return lookup_dict.index(key)
        
        for row in csvreader:
            job_id = row[_('task_id')]
            if job_id not in data['jobs']:
                data['jobs'] = dict()
                data['jobs']['sets'] = dict()

            set_id = row[_('id')]
            if set_id not in data['sets']:
                data['jobs']['sets'][set_id] = dict()         
            
            set = data['jobs']['sets'][set_id]

            event = row[_('event')]
            timestamp = float(row[_('timestamp')])
            if event == "SET_DEFINED":
                set['defined'] = timestamp
                set['size'] = int(row[_('size')])
                set['model'] = dict()
                set['model_params'] = dict()
            
            set['model'][timestamp] = row[_('model')]
            set['model_params'][timestamp] = row[_('model_params')]
            
    data['num_jobs'] = len(data['jobs'])
    for key in data['jobs']:
        data['jobs'][key]['num_sets'] = len(data['jobs'][key]['sets'])

    
    return data

def load_policy_data(dir):
    
    data = dict()
    data["start_time"] = 0
    data["end_time"] = 0
    data['frames'] = dict()
    frame_list = list()

    frame0 = dict()
    frame0['eval_start'] = 0
    frame0['eval_end'] = 0
    frame0['frame_end'] = 0
    frame0['frame_duration'] = 0
    frame0['eval_duration'] = 0
    frame0['gain'] = 0
    frame0['normalized_gain'] = 0
    frame0['system_perf'] = 0
    frame0['setops'] = dict()

    
    frame_list.append(frame0)

    start_time = 0
    #timestamp,event,setop_results,gain,gain_normalized   
    with open(join(dir, "policy.csv"), newline='') as csvfile:
        csvreader = csv.reader(csvfile)
        header = next(csvreader)
        

        def _(key, lookup_dict=header):
            return lookup_dict.index(key)
        
        frame_start = None
        frame = None
        for row in csvreader:
            timestamp = float(row[_('timestamp')])
            if data["start_time"] == 0:
                data["start_time"] = timestamp
            if timestamp > data["end_time"]:
                data["end_time"] = timestamp
            event = row[_('event')]

            if event == "POLICY_EVALUATION_START":
                if start_time == 0:
                    start_time = timestamp
                frame_start = timestamp - start_time
                frame = dict()
                data['frames'][frame_start] = frame
                frame['eval_start'] = frame_start
                
            elif event == "POLICY_EVALUATED":
                frame['eval_end'] = timestamp - start_time
                frame['eval_duration'] = frame['eval_end'] - frame['eval_start']
                frame['gain'] = float(row[_('gain')])
                frame['normalized_gain'] = float(row[_('gain_normalized')])
                
                setop_strings = [('{"setop_id"'+elem).rstrip(';') for elem in row[_('setop_results')].split('{"setop_id"')][1:]
                setops = [json.loads(op) for op in setop_strings]
                frame['setops'] = dict()
                for setop in setops:
                    entry = dict()
                    entry['num_nodes_before'] = len(setop['nodes_before'].split(';'))
                    entry['num_nodes_after'] = len(setop['nodes_after'].split(';'))
                    entry['perf_before'] = setop['speedup_before']
                    entry['perf_after'] = setop['speedup_after']

                    frame['setops'][setop['setop_id']] = entry
                
                prev_frame = frame_list[-1]
                prev_frame['frame_end'] = frame['eval_end']
                prev_frame['duration'] = prev_frame['frame_end'] - prev_frame['eval_end']

                frame['system_perf'] = prev_frame['system_perf'] + frame['gain']

                frame_list.append(frame)
        frame_list[-1]['frame_end'] = frame_list[-1]['eval_end']
        frame_list[-1]['duration'] = 0

    data['avg_system_perf'] = sum([frame['system_perf']*frame['duration'] for frame in data['frames'].values()]) \
                            / sum([frame['duration'] for frame in data['frames'].values()])

    return data
                    
def load_iter_data(file):
    if not exists(file):
        return None
    
    frames = dict()
    print("open file "+file)
    with open(file, newline='') as csvfile:
        # Create a CSV reader object
        csvreader = csv.reader(csvfile)
        header = next(csvreader)
        print(header)
        def _(key, lookup_dict=header):
            return lookup_dict.index(key)
        iters = dict() 
        # Read each row from the CSV file
        for row in csvreader:
            print(row)
            event = row[_('event_name')]
            iter = row[_('iteration')]
            num_procs = int(row[_('num_procs')])
            timestamp = int(row[_('#timestamp')])
            if iter not in frames:
                frames[iter] = dict()
            if event == 'adapt_start':
                frames[iter]['adapt_start'] = timestamp 
                frames[iter]['adapt_start_procs'] =  num_procs
            elif event == 'adapt_end':
                frames[iter]['adapt_end'] =  timestamp
                frames[iter]['adapt_end_procs'] =  num_procs
                if 'adapt_start' in frames[iter]:
                    frames[iter]['adapt_overhead'] = frames[iter]['adapt_end'] - frames[iter]['adapt_start']
                    frames[iter]['adapt_delta_procs'] = num_procs - frames[iter]['adapt_start_procs']
            elif event == 'work_start':
                frames[iter]['work_start'] = timestamp 
                frames[iter]['work_procs'] =  num_procs
            elif event == 'work_end':
                frames[iter]['work_end'] =  timestamp
                if 'work_start':
                    frames[iter]['work_duration'] = frames[iter]['work_end'] - frames[iter]['work_start']
            elif event == 'redist_start':
                frames[iter]['redist_start'] = timestamp 
                frames[iter]['redist_procs'] =  num_procs
            elif event == 'redist_end':
                frames[iter]['redist_end'] =  timestamp
                frames[iter]['redist_duration'] = frames[iter]['redist_end'] - frames[iter]['redist_start']
            elif event == 'iter_start':
                frames[iter]['iter_start'] = timestamp 
                frames[iter]['iter_start_procs'] =  num_procs
            elif event == 'iter_end':
                frames[iter]['iter_end'] =  timestamp
                frames[iter]['iter_end_procs'] =  num_procs
                if 'iter_start' in frames[iter]:
                    frames[iter]['iter_duration'] = frames[iter]['iter_end'] - frames[iter]['iter_start']                                
                    frames[iter]['iter_delta_procs'] =  frames[iter]['iter_end_procs'] - frames[iter]['iter_start_procs']


    return frames