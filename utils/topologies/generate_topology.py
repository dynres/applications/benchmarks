import os
import yaml
import sys

def generate_topology_slurm(output_file, num_nodes=-1, cores_per_node=-1):
    # Initialize an empty dictionary to store the topology data
    topology = {'topology': {'nodes': {}}}
    nodes = []
    # Get the SLURM_NODELIST environment variable
    breakout = False
    slurm_nodelist = os.environ.get('SLURM_NODELIST')
    for shared_prefix in slurm_nodelist.split(',i'):
        if breakout:
            break
        if not shared_prefix.startswith('i'):
            shared_prefix = 'i'+shared_prefix
        prefix = shared_prefix.split('s')[0]+"s"
        intervals = shared_prefix.split('s')[1].strip("[").strip("]").split(',')
        for interval in intervals:
            if breakout:
                break
            interval = interval.strip("]")
            node_numbers = interval.split('-')
            if len(node_numbers) == 1:
                nodes.append(prefix+node_numbers[0])
                if -1 != num_nodes and len(nodes == num_nodes):
                    breakout = True
                continue
            for n in range(int(node_numbers[0]), int(node_numbers[1]) + 1):
                index = str(n) if n > 9 else "0"+str(n)
                nodes.append(prefix+index)
                if -1 != num_nodes and len(nodes == num_nodes):
                    breakout = True
                    break

    # Get the SLURM_JOB_CPUS_PER_NODE environment variable
    if -1 == cores_per_node: 
        cores_per_node = os.environ.get('SLURM_NTASKS_PER_NODE')

    # Iterate through the nodes and cpus_per_node lists simultaneously
    for node in nodes:
        # Add the node to the topology dictionary
        topology['topology']['nodes'][node] = {'num_cores': int(cores_per_node)}

    # Write the topology dictionary to a YAML file
    with open(output_file, 'w') as f:
        yaml.dump(topology, f)

def generate_topology_docker(output_file, num_nodes=1, cores_per_node=1):
    topology = {'topology': {'nodes': {}}}
    for n in range(1, num_nodes + 1):
        topology['topology']['nodes']['n'+str(n)] = {'num_cores': int(cores_per_node)}
    with open(output_file, 'w') as f:
        yaml.dump(topology, f)

def generate_topology_oar(output_file, num_nodes=-1, cores_per_node=-1):
    pass

if __name__ == '__main__':
    # Check if the output file argument is provided
    if len(sys.argv) != 3:
        print('Usage: python generate_topolog.py <target> <output_file>')
        sys.exit(1)

    # Get the output file name from the command line argument
    target = sys.argv[1]
    output_file = sys.argv[2]

    # Call the create_topology function for the specified target
    if target == 'slurm':
        create_topology_slurm(output_file)
    elif target == 'docker':
        create_topology_docker(output_file)
    elif target == 'oar':
        create_topology_oar(output_file)
    else:
        print("ERROR: Creating topology for target '"+target+"' is not supoorted!")
        print("EXIT")

