#!/bin/bash

#if [ $# -ne 3 ]; then
#        echo "USAGE: ./workload-generator.sh 'workload name' 'number of jobs' 'inter-arrival time'"
#        exit 1
#fi

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

echo "Compile ..."
gcc ${SCRIPT_DIR}/model.c -lm -DMAKE_PLOTS -DLEN=$3 -DARR_FACTOR=$4 -DMAX_TIME=$5 -DTIME_LIM=$6 -DFACTOR=$7 -DMAX_SIZE=$8 -o ${SCRIPT_DIR}/model
echo "Run ..."
${SCRIPT_DIR}/model $1 > "$1/$2"

#python3 workloadFromSwf.py $1
