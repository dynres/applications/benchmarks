import inspect
import os
import importlib

def supports_argument(func, arg_name, index):
    sig = inspect.signature(func)
    func_params = sig.parameters

    kwarg=False
    if arg_name.endswith("="):
        kwarg = True
        arg_name = arg_name[-1]
    if arg_name in func_params:
        if index != func_params[arg_name]:
            return False
        param = func_params[arg_name]
        if kwarg and (param.default == inspect.Parameter.empty):
            return False
        if param.kind not in (inspect.Parameter.VAR_POSITIONAL, inspect.Parameter.VAR_KEYWORD):
            return True
    return False

def import_templates(dir: str, template_name, functions=[], args=[], vars= []):
    modules = []
    valid_modules = []
    for filename in os.listdir(dir):
        if template_name != 'all' and filename != template_name+".py":
            continue

        if filename.endswith(".py") and filename != "__init__.py":
            module_name = filename[:-3]
            module_path = os.path.join(dir, filename)

            spec = importlib.util.spec_from_file_location(module_name, module_path)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)

            modules.append(module)
    
    for module in modules:
        for function, func_args in zip(functions, args):
            if not hasattr(module, functions):  # Check if the module has the desired function
                print(f"Module {module.__name__} does not have '"+ function +"'")
                return []
            for arg, index in zip(func_args, range(len(func_args))):
                if not supports_argument(function, args, index):
                    print(f"Module {module.__name__}'s function '"+ function +"' does not suport arg '"+arg+"'")
                    return []
        for var in vars:
            if None == getattr(module, var, None):
                print(f"Module {module.__name__} attribute '"+var+"' not found")
                return  []
            elif callable(getattr(module, var, None)):
                print(f"Module {module.__name__} attribute '"+var+"' is a function. Should be a variable")
                return  []
        valid_modules.append(module)

    return modules