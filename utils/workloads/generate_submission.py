import os
import pathlib
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from templates.load import import_templates
from utils.metadata.metadata_generation import *

synthetic_executable_template = \
"""#!/bin/bash
echo 'EXECUTEABLE' >> /opt/hpc/build/testest.txt

SCRIPT_DIR=$( cd -- "$( dirname -- "${{BASH_SOURCE[0]}}" )" &> /dev/null && pwd )

export ASAN_OPTIONS=detect_leaks=0:detect_odr_violation=0:verbosity=3

bench_sleep --output $BENCHMARKS_ROOT/{timestamp_file} --rm dyn_rm --inhibitor {inhibitor} --blocking {blocking} --iterations {iterations} --t_s {t_s} --t_p {t_p} --monitoring {enable_monitoring} --generator_key replace_generator >> $BENCHMARKS_ROOT/{output_file} 2>&1
exit 0
"""

submission_template = \
"""
from dyn_rm.mca.base.system.module.tasks.task import MCATaskModule
from dyn_rm.mca.system.modules.psets.pset_models import *
from dyn_rm.mca.system.modules.psets.psetop_models.output_space_generators import *

from functools import partial

from os.path import join,dirname
from os import environ


def create_task_graph_function(task_graph, params):

    # Create Task 1
    task1 = MCATaskModule('{job_name}', join(environ["BENCHMARKS_ROOT"],'{executable}'))
    task1.run_service('SET', 'TASK_EXECUTION_ARGUMENTS', [])
    task1.run_service('SET', 'TASK_LAUNCH_OUTPUT_SPACE_GENERATOR', 
                        partial(output_space_generator_launch, 
                              task=task1, 
                              model={model}, 
                              model_params={{'t_s': {t_s}, 't_p' : {t_p}}}, 
                              num_delta={num_delta},
                              num_max={num_max},
                              num_min={num_min},
                              power_of_two={power_of_two},
                              mapping = '{mapping}'))
    task1.run_service('SET', 'ATTRIBUTE', 'replace_generator', 
                        partial(output_space_generator_replace,
                                task=task1,
                                model={model}, 
                                model_params={{'t_s': {t_s}, 't_p' : {t_p}}},
                                num_delta_add={num_delta_add},
                                num_delta_sub={num_delta_sub},
                                num_max={num_max},
                                num_min={num_min},
                                power_of_two={power_of_two},
                                factor={factor},
                                mapping = '{mapping}'))

    task_graph.run_service('SET', 'ATTRIBUTE', 'ESTIMATED_RUNTIME', {runtime})
    task_graph.run_service('SET', 'ATTRIBUTE', MCATaskModule.TASK_ATTRIBUTE_REQ_NODES, {num_nodes}) 

    task_graph.run_service('ADD', 'TASKS', [task1])

    return task_graph
"""
def is_key_in_template(template, key):
    placeholder = f"{{{key}}}"
    return placeholder in template

def get_relative_path(base_dir, path):
    path = pathlib.Path(path)
    try:
        parts = list(path.parts)
        index = parts.index(base_dir.split(os.sep)[-1])
        return str(pathlib.Path(*parts[index + 1:]))
    except ValueError:
        return str(path)

def generate_executable(name,
                        executable_template,
                        dir=".",
                        out_dir=".",
                        enable_monitoring=False,
                        **kwargs):
    
    root_dir = os.environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set")

    m = 1 if enable_monitoring else 0

    # filter out values not supported by template
    invalid_keys = []
    for key in kwargs:
        if not is_key_in_template(executable_template, key):
            invalid_keys.append(key)
    for key in invalid_keys:
        kwargs.pop(key, None)
    data = executable_template.format(output_file = get_relative_path(root_dir, os.path.join(out_dir, os.path.splitext(name.split('/')[-1])[0]+".out")),
                                      enable_monitoring=m,
                                      **kwargs)
    filename = os.path.join(root_dir, dir, name)
    with open(filename, 'w') as file:
        file.writelines(data)
    
    os.chmod(filename, 0o755)
    

    return get_relative_path(root_dir, filename) 

def generate_synthetic_executable(name,
                        dir=".",
                        out_dir=".",
                        t_s=3, 
                        t_p=1,  
                        blocking=False, 
                        iterations=100,
                        enable_monitoring=False,
                        inhibitor=1,
                        verbosity=0):
    
    root_dir = os.environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set")    
    
    b = 1 if blocking else 0

    if verbosity > 2:
        print("Generating synthetic executable @: '"+dir+name+"'")
    return generate_executable( name, 
                                synthetic_executable_template,
                                dir=dir,
                                out_dir=get_relative_path(root_dir, out_dir),
                                timestamp_file=get_relative_path(root_dir, os.path.join(out_dir, os.path.splitext(name)[0]+".csv")),
                                blocking=b,
                                enable_monitoring=enable_monitoring,
                                iterations=iterations,
                                inhibitor=inhibitor,
                                t_s=t_s,
                                t_p=t_p)

def generate_executable_from_template(name, dir, out_dir, collection, template_dir, template_name, 
                           enable_monitoring = False, **kwargs):
    root_dir = os.environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set")  
    
    templates = import_templates(os.path.join(root_dir, 'experiments', collection, 'templates', template_dir),
                                template_name,
                                vars=["EXECUTABLE_TEMPLATE", 
                                      "EXECUTABLE_ARGUMENTS",
                                      "SUBMISSION_ARGUMENTS"]
                                )
    if len(templates) == 0:
        print("Unable to find template '"+os.path.join(root_dir, 'experiments', collection, 'templates', template_dir, template_name)+"'")
        return None
    
    exec_template = None
    with open(os.path.join(root_dir, 'applications', 'templates', getattr(templates[0], "EXECUTABLE_TEMPLATE", None)), "r") as file:
        exec_template = file.read()
    exec_arguments = getattr(templates[0], "EXECUTABLE_ARGUMENTS", None)
    
    for arg in kwargs.keys():
        exec_arguments[arg] = kwargs[arg]
    exec_arguments.pop("enable_monitoring", None)
    exec_arguments["timestamp_file"] = get_relative_path(root_dir, os.path.join(out_dir, os.path.splitext(name)[0]+".csv"))

    executable = generate_executable(name, exec_template, dir, out_dir, enable_monitoring=enable_monitoring,
                                     **exec_arguments)
    
    return executable

def generate_submission_from_template(name, dir, out_dir, collection, template_dir, template_name, 
                           executable, enable_monitoring = False, **kwargs):
    root_dir = os.environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set")  
    
    templates = import_templates(os.path.join(root_dir, 'experiments', collection, 'templates', template_dir),
                                template_name,
                                vars=["EXECUTABLE_TEMPLATE", 
                                      "EXECUTABLE_ARGUMENTS",
                                      "SUBMISSION_ARGUMENTS"]
                                )
    if len(templates) == 0:
        return None
    
    submission_arguments = getattr(templates[0], "SUBMISSION_ARGUMENTS", None)

    for arg in kwargs.keys():
        submission_arguments.pop(arg, None)
    submission = generate_submission(dir, name, executable, **submission_arguments, **kwargs)
    
    return submission


def generate_submission(dir, job_name, executable, model="AmdahlPsetModel",
                          t_s=1, t_p=300, num_delta=-1, factor=-1,
                          mapping='dense', num_delta_add=-1, 
                          num_delta_sub=-1, power_of_two=False, 
                          num_max=sys.maxsize, num_min=-1, runtime=1200, num_nodes=4,
                          verbosity=0):
    
    root_dir = os.environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set")
    
    filename = os.path.join(root_dir, dir, job_name+".batch")
    
    if verbosity > 2:
        print("Generating submission file @: '"+str(filename)+"'")
    with open(filename, "w") as f:
        f.writelines(submission_template.format(
            job_name=job_name,
            executable=executable,
            t_s=t_s,
            t_p=t_p,
            model=model,
            num_delta=num_delta,
            num_max=num_max,
            num_min=num_min,
            power_of_two=power_of_two,
            mapping=mapping,
            num_delta_add=num_delta_add,
            num_delta_sub=num_delta_sub,
            factor=factor,
            runtime=runtime,
            num_nodes=num_nodes
        ))
    return get_relative_path(root_dir, filename)


def generate_single_app_run(name, 
                            num_nodes, 
                            cores_per_node, 
                            ram_per_node, 
                            overwrite,
                            timeout, 
                            verbosity,
                            policy,
                            policy_params,
                            plots,
                            plot_params,
                            collection,
                            template_dir,
                            template_name,
                            executable_params,
                            submission_params):
    base_name = name + '_n'+str(num_nodes) + '_c'+str(cores_per_node)
    try:
        _, submission_dir, _, output_dir = exp_meta_data_create(base_name, 
                                                                num_nodes, 
                                                                cores_per_node,
                                                                timeout=timeout,
                                                                policy=policy,
                                                                policy_params=policy_params, 
                                                                plots=plots,
                                                                plot_params=plot_params,
                                                                overwrite=overwrite) 
    except ExpDirExistsExecption as e:
        print(e)
        return

    executable = generate_executable_from_template(name, submission_dir, output_dir, 
                                                   collection, template_dir, template_name, 
                                                   **executable_params)
    if None == executable:
        if verbosity > 0:
            print("An error occured in 'generate_executable_from_template'")
        exp_meta_data_set(base_name, ["status"], [ExpMetaDataStatus.STATUS_GENERATION_FAILED])
        return None
    exp_meta_data_append(base_name, "executables", [executable])

    submission = generate_submission_from_template(base_name, submission_dir, output_dir, 
                                                   collection, template_dir, template_name,
                                                   executable, **submission_params)
    if None == submission:
        if verbosity > 0:
            print("An error occured in 'generate_submission_from_template'")
        exp_meta_data_set(base_name, ["status"], [ExpMetaDataStatus.STATUS_GENERATION_FAILED])
        return None
    exp_meta_data_append(base_name, "batch_scripts", [submission])

    exp_meta_data_complete(base_name)

    return base_name


def generate_compound(name, num_nodes, cores_per_node, ram_per_node, 
                            overwrite, timeout, verbosity, 
                            plots, plot_params,
                            collection, names):
    base_name = name + '_n'+str(num_nodes) + '_c'+str(cores_per_node)
    try:
        _, submission_dir, _, output_dir = exp_meta_data_create(base_name, 
                                                                num_nodes, 
                                                                cores_per_node,
                                                                type='compound',
                                                                timeout=timeout,
                                                                policy=None,
                                                                policy_params={}, 
                                                                plots=plots,
                                                                plot_params=plot_params,
                                                                overwrite=overwrite,
                                                                sub_collection=collection,
                                                                sub_names=names) 
    except ExpDirExistsExecption as e:
        print(e)
        return

    exp_meta_data_complete(base_name)

    return base_name