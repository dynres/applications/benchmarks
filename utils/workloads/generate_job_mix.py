import os
import subprocess
import random
import importlib.util
import inspect
import sys
import pathlib

sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from generate_submission import generate_submission, generate_synthetic_executable, generate_executable
from templates.load import import_templates 
from utils.metadata.metadata_generation import *

def get_relative_path(base_dir, path):
    path = pathlib.Path(path)
    try:
        parts = list(path.parts)
        index = parts.index(base_dir.split(os.sep)[-1])
        return str(pathlib.Path(*parts[index + 1:]))
    except ValueError:
        return str(path)

def get_closest(key, data):
        # Find the closest key in the dictionary
        closest_key = min([_k for _k in data.keys() if _k >=key], key=lambda k: abs(k - key))
        return data[closest_key]

def parse_line(line):
    fields = line.split()
    return int(fields[0]), float(fields[1]), float(fields[3]), int(fields[4])

def generate_swf(swf_file, 
                 swf_dir,
                 num_jobs, 
                 num_nodes,
                 inter_arrival,
                 n_time,
                 max_time,
                 factor,
                 verbosity=0):
    if 0 < verbosity:
        print("Generating SWF Workload: '"+swf_file+"'")

    if 10 < verbosity:
        print("Num Jobs: "+num_jobs)
        print("Num Nodes: "+num_nodes)
        print("Inter-arrival: "+inter_arrival)
        print("Num Time bins: "+n_time)
        print("Max runtime: "+max_time)
        print("Factor: "+factor)
    
    root_dir = os.environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set")
    
    
    try:
        result = subprocess.run([os.path.join(root_dir, "utils", "workloads", "feitelson_model", "workload-generator.sh"),
                                 swf_dir,
                                 swf_file, 
                                 str(num_jobs),
                                 str(inter_arrival),
                                 str(n_time),
                                 str(max_time),
                                 str(factor),
                                 str(num_nodes)
                                 ], check=True, capture_output=True, text=True)
        if 20 < verbosity:
            print("Output:", result.stdout)
            print("Error:", result.stderr)
    except subprocess.CalledProcessError as e:
        print("Error executing feitelson workload generator:", e)
        return None
    return os.path.join(swf_dir,swf_file)

def generate_synthetic_mix_from_swf(dir, 
                              mix_name, 
                              swf_file,
                              out_dir=".", 
                              t_iter=10,
                              mapping='dense',
                              enable_monitoring=False, 
                              efficiency=0.9,
                              cores_per_node=8, 
                              verbosity=0):    
    root_dir = os.environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set")
    
    if 0 < verbosity:
        print("Generating job mix '"+mix_name+"' in directory '"+dir+"'")

    data = []
    executables = []
    submissions = []
    
    # Generate submission file and executable for each job in the workload
    with open(swf_file, 'r') as workload:
        for line in workload:
            # Skip comment lines and empty lines (those with only spaces or no content)
            if line.startswith(";") or not line.strip():
                continue
            id, arrival, runtime, job_size = parse_line(line)

            # Dertmine t_s and t_p for given runtime, num_procs and parallel efficiency
            num_procs = job_size * cores_per_node
            t_s = ( (runtime * num_procs * (1 - efficiency)) / (num_procs - 1) ) 
            t_p = ( runtime * num_procs * (1 - (num_procs * (1 - efficiency)) / (num_procs - 1) ))
            
            print("Original t_s "+str(t_s))
            print("Original t_p "+str(t_p))

            # Users specify the max iteration time (= max reaction time) with t_iter
            # Asuming worst case of sequnetial execution determin number of required iterations 
            num_iters = (t_s + t_p/cores_per_node)/t_iter
            
            # Scale down t_s and t_p to iteration times
            t_s /= num_iters
            t_p /= num_iters

            # Generate excutable
            executable = generate_synthetic_executable( mix_name+ "_"+str(id),
                                                        dir=dir,
                                                        out_dir=out_dir,
                                                        iterations=num_iters,
                                                        t_s=t_s,
                                                        t_p=t_p,
                                                        inhibitor=-t_iter,
                                                        enable_monitoring=enable_monitoring,
                                                        verbosity=verbosity)
            executables.append(executable)

            # Generate submission
            submission = generate_submission(dir,
                                             mix_name+"_"+str(id),
                                             executable,
                                             t_s=t_s,
                                             t_p=t_p,
                                             mapping=mapping,
                                             runtime=runtime,
                                             num_nodes=job_size,
                                             verbosity=verbosity)
            submissions.append(submissions)
            
            params = "\"{'prefix_generator' : '__import__(\\'os\\').environ.get(\\'BENCHMARKS_ROOT\\', \\'\\')'}\""
            data.append(str(arrival)+","+submission+","+params+"\n")
            print(str(arrival)+","+submission+","+params+"\n")
    
    # Now write data into file
    mix_filename = os.path.join(dir, mix_name+".mix")
    with open(mix_filename, 'w') as mix_file:
        data[-1] = data[-1].replace("\"{'prefix_generator' : '__import__(\\'os\\').environ.get(\\'BENCHMARKS_ROOT\\', \\'\\')'}\"", "\"{'terminate_soon':True, 'prefix_generator' : '__import__(\\'os\\').environ.get(\\'BENCHMARKS_ROOT\\', \\'\\')'}\"")
        mix_file.writelines(data)
    
    return get_relative_path(root_dir, mix_filename), submissions, executables

def generate_synthetic_mix(dir, 
                     mix_name, 
                     num_jobs,
                     out_dir=".", 
                     enable_monitoring=False, 
                     t_iter=10,
                     efficiency=0.9,
                     mapping='dense', 
                     num_nodes=64,
                     num_cores=8, 
                     inter_arrival=10, 
                     n_time=64, 
                     max_time=100000000, 
                     factor=1, 
                     verbosity=0):
    
    # Create dir for job mix statistics
    swf_dir = os.path.join(dir, "swf")
    try:
        os.makedirs(swf_dir, mode=0o755, exist_ok=True)
    except OSError as e:
        print(f"Error creating directories: {e}")
        return -1
    
    if verbosity > 0:
        print("Generating swf file")

    swf = generate_swf('workload.swf', 
                 swf_dir,
                 num_jobs, 
                 num_nodes,
                 inter_arrival,
                 n_time,
                 max_time,
                 factor,
                 verbosity=0)
    
    if verbosity > 0:
        print("Generating synthetic mix from swf file")

    return generate_synthetic_mix_from_swf(dir, 
                                     mix_name, 
                                     swf,
                                     out_dir=out_dir,
                                     mapping=mapping,
                                     t_iter=t_iter,
                                     efficiency=efficiency,
                                     enable_monitoring=enable_monitoring,
                                     cores_per_node=num_cores,
                                     verbosity=verbosity)

def generate_app_mix_from_swf(dir,
                              out_dir, 
                              mix_name, 
                              swf,
                              apps_template_dir,
                              efficiency=0.9,
                              enable_monitoring=False,
                              cores_per_node=1,
                              verbosity=0):    
    
    if 0 < verbosity:
        print("Generating app mix '"+mix_name+"' in directory '"+dir+"'")

    root_dir = os.environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set") 

    data = []
    submissions = []
    executables = []
    
    templates = import_templates(   os.path.join(os.path.dirname(__file__), 'templates', apps_template_dir),
                                    'all',
                                    vars=["EXECUTABLE_TEMPLATE", 
                                          "EXECUTABLE_ARGUMENTS",
                                          "SUBMISSION_ARGUMENTS"]
                                    )

    # Generate submission file and executable for each job in the workload
    with open(swf, 'r') as workload:
        for line in workload:
            # Skip comment lines and empty lines (those with only spaces or no content)
            if line.startswith(";") or not line.strip():
                continue
            id, arrival, _, __ = parse_line(line)
            app_id = random.randint(0, len(templates) - 1)
            template = templates[app_id]

            # Generate excutable
            filename = os.path.join(dir, mix_name+"_job"+str(id)+".sh")
            exec_template = None
            with open(os.path.join(root_dir, 'applications', 'templates', getattr(template, "EXECUTABLE_TEMPLATE", None)), "r") as file:
                exec_template = file.read()
            executable_arguments = getattr(template, "EXECUTABLE_ARGUMENTS").copy()
            executable_arguments['enable_monitoring'] = enable_monitoring
            executable_arguments['timestamp_file'] = os.path.join(out_dir, mix_name+"_job"+str(id)+".csv")
            executable = generate_executable(filename,
                                             exec_template,
                                             dir,
                                             out_dir,
                                             **executable_arguments
                                             )
            executables.append(executable)

            # Generate submission
            # Use template arguments but set custom values for:
            #   - num_nodes
            #   - runtime
            #   - verbosity
            #   - t_s, t_p

            submission_arguments = getattr(template, "SUBMISSION_ARGUMENTS").copy()
            # Get some submission arguments from the template
            mapping = submission_arguments["mapping"]
            if mapping == 'sparse':
                cores_per_node = 1
            elif mapping.endswith(':node'):
                cores_per_node = int(mapping.split(':')[0]) 

            base_size = submission_arguments["num_nodes"] * cores_per_node
            base_time = submission_arguments["runtime"]
            t_s = submission_arguments["t_s"]
            t_p = submission_arguments["t_p"]

            # Caluclate job size & runtime based on efficiency, t_s and t_p
            job_size = int((t_s + t_p)/(t_s * efficiency) + t_p / t_s)
            base_speedup = (t_s + t_p) / (t_s + t_p / base_size)
            new_speedup = (t_s + t_p) / (t_s + t_p / job_size)
            runtime = (new_speedup / base_speedup) * base_time

            for key in ["num_nodes", "runtime", "verbosity", "t_s", "t_p"]:
                submission_arguments.pop(key, None)
            
            # TODO: Find default values that make sense
            if enable_monitoring:
                t_s = 1
                t_p = 300

            submission = generate_submission(   dir, 
                                                mix_name+"_"+str(id),
                                                executable,
                                                t_s=t_s,
                                                t_p=t_p,
                                                runtime=runtime,
                                                num_nodes=job_size,
                                                verbosity=verbosity,
                                                **submission_arguments)
            submissions.append(executable)

            params = "\"{'prefix_generator' : '__import__(\\'os\\').environ.get(\\'BENCHMARKS_ROOT\\', \\'\\')'}\""
            data.append(str(arrival)+","+submission+","+params+"\n")
    
    # Now write data into file
    mix_filename = os.path.join(dir, mix_name+".mix")
    with open(mix_filename, 'w') as mix_file:
        data[-1] = data[-1].replace("\"{'prefix_generator' : '__import__(\\'os\\').environ.get(\\'BENCHMARKS_ROOT\\', \\'\\')'}\"", "\"{'terminate_soon':True, 'prefix_generator' : '__import__(\\'os\\').environ.get(\\'BENCHMARKS_ROOT\\', \\'\\')'}\"")
        mix_file.writelines(data)

    return mix_filename, submission, executables

def generate_job_mix_from_apps( dir, 
                                out_dir,
                                mix_name, 
                                num_jobs,
                                num_nodes,
                                cores_per_node,
                                apps_template_dir, 
                                max_time=3600,
                                enable_monitoring=False, 
                                efficiency=0.9,
                                inter_arrival=10,
                                verbosity=0):

    # Create dir for job mix statistics
    swf_dir = os.path.join(dir, "swf")
    try:
        os.makedirs(swf_dir, mode=0o755, exist_ok=True)
    except OSError as e:
        print(f"Error creating directories: {e}")
        return -1
    
    if verbosity > 0:
        print("Generating swf file")
    swf = generate_swf( 'workload.swf',
                        swf_dir,
                        num_jobs, 
                        num_nodes,
                        inter_arrival,
                        num_nodes,
                        max_time,
                        1,
                        verbosity=0)
    
    return generate_app_mix_from_swf(dir,
                                     out_dir, 
                                     mix_name, 
                                     swf,
                                     apps_template_dir,
                                     efficiency=efficiency,
                                     enable_monitoring=enable_monitoring,
                                     cores_per_node=cores_per_node,
                                     verbosity=verbosity)
    

def generate_synthetic_job_mix_run(name, num_nodes=8, cores_per_node=8, ram_per_node=1, 
                                   num_jobs=100, inter_arrival=10, pe=0.9, t_iter=10,
                                   mapping='dense', max_time=1000, enable_monitoring=False, timeout=3600,
                                   policy="DiscreteSteepestAscend", policy_params={'step_size' : 'power_of_2'}, plots=[], 
                                   overwrite=False, verbosity=0):

    base_name = name + '_n'+str(num_nodes) + '_c'+str(cores_per_node)
    try:
        _, submission_dir, _, output_dir = exp_meta_data_create(base_name, 
                                                                num_nodes, 
                                                                cores_per_node,
                                                                timeout=timeout,
                                                                policy=policy,
                                                                policy_params=policy_params, 
                                                                plots=plots,
                                                                overwrite=overwrite) 
    except ExpDirExistsExecption as e:
        print(e)
        return

    mix_name, submissions, executables =\
          generate_synthetic_mix(submission_dir, 
                     base_name, 
                     num_jobs,
                     out_dir=output_dir, 
                     enable_monitoring=enable_monitoring, 
                     t_iter=t_iter,
                     efficiency=pe,
                     mapping=mapping, 
                     num_nodes=num_nodes,
                     num_cores=cores_per_node, 
                     inter_arrival=inter_arrival, 
                     n_time=num_nodes, 
                     max_time=max_time, 
                     factor=1, 
                     verbosity=verbosity)

    #exp_meta_data_append(base_name, "executables", executables)
    #exp_meta_data_append(base_name, "batch_scripts", submissions)
    exp_meta_data_append(base_name, "mix_scripts", [mix_name])

    exp_meta_data_complete(base_name)

    return base_name


def generate_job_mix_run(name, collection, apps_template_dir, num_nodes=8, cores_per_node=8, ram_per_node=1, 
                                   num_jobs=100, inter_arrival=10, pe=0.9,
                                   max_time=1000, enable_monitoring=False, timeout=3600,
                                   policy="DiscreteSteepestAscend", policy_params={'step_size' : 'power_of_2'}, 
                                   plots=[], plot_params=[],
                                   overwrite=False, verbosity=0):

    root_dir = os.environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set")  

    base_name = name + '_n'+str(num_nodes) + '_c'+str(cores_per_node)
    try:
        _, submission_dir, _, output_dir = exp_meta_data_create(base_name, 
                                                                num_nodes, 
                                                                cores_per_node,
                                                                timeout=timeout,
                                                                policy=policy,
                                                                policy_params=policy_params, 
                                                                plots=plots,
                                                                plot_params=plot_params,
                                                                overwrite=overwrite) 
    except ExpDirExistsExecption as e:
        print(e)
        return

    dir = os.path.join(root_dir, "experiments", collection, "templates", apps_template_dir)
    print(output_dir)
    mix_name, submissions, executables =\
        generate_job_mix_from_apps( os.path.join(root_dir, submission_dir),
                                    output_dir, 
                                    base_name, 
                                    num_jobs,
                                    num_nodes,
                                    cores_per_node,
                                    apps_template_dir=dir, 
                                    enable_monitoring=enable_monitoring,
                                    efficiency=pe,
                                    inter_arrival=inter_arrival,
                                    max_time=max_time,
                                    verbosity=verbosity)

    #exp_meta_data_append(base_name, "executables", executables)
    #exp_meta_data_append(base_name, "batch_scripts", submissions)
    print(mix_name)
    exp_meta_data_append(base_name, "mix_scripts", [mix_name])

    exp_meta_data_complete(base_name)

    return base_name