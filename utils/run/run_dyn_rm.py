from dyn_rm.mca.base.system.module import *
from dyn_rm.mca.system.modules.topology import DefaultTopologyGraphCreationModule
from dyn_rm.mca.system.modules.tasks import DefaultTaskGraphCreationModule
from dyn_rm.mca.submission.modules.default import DefaultSubmissionModule
from dyn_rm.mca.base.resource_manager.module import MCAResourceManagerModule
from dyn_rm.mca.system.modules.system import PrrteSingleInstanceSystem,PrrteMultipleInstancesSystem
from dyn_rm.mca.policy.modules import Discrete_Steepest_Ascend,EasyBackfilling,FirstFitFirst

from dyn_rm.mca.base.callback.component import MCACallbackComponent
from dyn_rm.mca.base.callback.module import MCACallbackModule

from dyn_rm.mca.mca import MCAClass
from dyn_rm.util.constants import *

from os.path import join,dirname
from time import sleep

import threading

import argparse
import sys

sleep_event = threading.Event()

def wakeup(conn_name, event_name):
    sleep_event.set()
    return DYNRM_MCA_SUCCESS

def wait_for_wakeup():
    sleep_event.wait()


def run_dyn_rm(submission_file, topo_file, output_dir, verbosity, policy, policy_params, system_class):

    header =    "\n\n=======================================\n"+    \
                "   STARTING DYN_RM RUN:\n"+                    \
                "       - Topology:     "+topo_file+"\n"+           \
                "       - Submission:   "+submission_file+"\n"+     \
                "       - Policy:       "+policy.__name__+"\n"+     \
                "       - Policy params:    "+str(policy_params)+"\n"+      \
                "       - System:       "+system_class.__name__+"\n"+ \
                "       - Verbosity:    "+str(verbosity)+"\n"+      \
                "       - Output dir:   "+output_dir+"\n"+          \
                "=======================================\n\n"
                
                
                

    print(header)
    sys.stdout.flush()

    # Create the resource manager for the system
    resource_manager = MCAResourceManagerModule(parent_dir = output_dir, enable_output=(output_dir!="none"), verbosity=verbosity)
    
    # Add a policy module
    resource_manager.run_service("ADD", "POLICY", "my_policy", policy, params = policy_params)
    
    # Add a system module for the given topology description using a certian TopologyGraphCreationModule
    resource_manager.run_service("ADD", "SYSTEM", "my_system", system_class, DefaultTopologyGraphCreationModule, topo_file)

    # register a callback to be executed when all jobs have been finalized 
    user = MCAClass()
    user.register_component(MCACallbackComponent())
    user.run_component_service(MCACallbackComponent, "REQUEST", "CONNECTION", MCACallbackModule, "RM", user, resource_manager, dict())
    user.run_component_service(MCACallbackComponent, "REGISTER", "CALLBACK", MCAResourceManagerModule.ALL_TASK_GRAPHS_TERMINATED, wakeup)

    # Submit the job/job mix using the given batch script parser (TaskGraphCreationModule)
    if submission_file.endswith('.batch'):
        resource_manager.run_service("SUBMIT", "OBJECT", "my_system", DefaultSubmissionModule, submission_file, {"task_graph_creation_modules" : [DefaultTaskGraphCreationModule], "terminate_soon" : True})
    elif submission_file.endswith('.mix'):
        resource_manager.run_service("SUBMIT", "MIX", "my_system", DefaultSubmissionModule, submission_file, {"task_graph_creation_modules" : [DefaultTaskGraphCreationModule]})
    
    if verbosity > 0:
        # print current system state
        system = resource_manager.run_service("GET", "SYSTEM", "my_system")
        system.run_service("PRINT", "SYSTEM")
    
    # Wait until the callback wakes us up
    wait_for_wakeup()

    if verbosity > 0:
        # print current system state
        system.run_service("PRINT", "SYSTEM")

    # Shutdown the resource manager
    resource_manager.run_service("MCA", "SHUTDOWN")

    header =    "\n\n=======================================\n"+    \
                "   FINISHED DYN_RM RUN:\n"+                    \
                "       - Topology:     "+topo_file+"\n"+           \
                "       - Submission:   "+submission_file+"\n"+     \
                "       - Policy:       "+policy.__name__+"\n"+     \
                "       - Policy params:    "+str(policy_params)+"\n"+      \
                "       - System:       "+system_class.__name__+"\n"+     \
                "       - Verbosity:    "+str(verbosity)+"\n"+      \
                "       - Output dir:   "+output_dir+"\n"+          \
                "=======================================\n\n"
    print(header)
    sys.stdout.flush()


def get_parameters():
    parser = argparse.ArgumentParser() # Add an argument
    parser.add_argument('--submission_file', type=str, required=True)
    parser.add_argument('--topology_file', type=str, required=True)
    parser.add_argument('--output_dir', type=str, required=False, default='none')
    parser.add_argument('--verbosity', type=int, required=False, default=0)
    parser.add_argument('--policy_params', type=str, required=False, default={'step_size' : 'linear'})
    parser.add_argument('--policy', type=str, required=False, default='Discrete_Steepest_Ascend')
    parser.add_argument('--system', type=str, required=False, default="PrrteSingleInstanceSystem")

    args = parser.parse_args()

    return  args.submission_file,\
            args.topology_file,\
            args.output_dir,\
            args.verbosity,\
            args.policy,\
            args.policy_params,\
            args.system
            

if __name__ == "__main__":
    print()
    print("================================================================================")
    print("Starting Experiment'")
    print("================================================================================")
    sys.stdout.flush()
    sub_file, topo_file, output_dir, verbosity, policy, policy_params, system = get_parameters()

    run_dyn_rm(sub_file, topo_file, output_dir, verbosity, eval(policy), eval(policy_params), eval(system))

    print()
    print("================================================================================")
    print("Finished Experiment successfully")
    print("================================================================================")
