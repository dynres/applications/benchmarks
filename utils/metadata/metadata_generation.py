import yaml
from os.path import join,dirname,exists,pathsep,isfile,islink,isdir,relpath
from os import makedirs, environ, sep, listdir, remove,walk,chdir,getcwd
import shutil
import pathlib
from contextlib import contextmanager
@contextmanager
def changedir(new_dir):
    """Context manager for changing the current working directory."""
    original_dir = getcwd()
    try:
        chdir(new_dir)
        yield
    finally:
        chdir(original_dir)

class ExpDirExistsExecption(Exception):
    message ="""
    ERROR: Directory '{0}' already exists!
    Please\n\t a) use a different name or \n\t b) rerun with 'override=True' to remove the existing directory
    """
    
    def __init__(self, dir):
        # Save the parameter for later use
        self.dir = dir
        # Optionally, call the base class constructor
        super().__init__(ExpDirExistsExecption.message.format(self.dir))

    def __str__(self):
        # Define the string representation of the exception
        return ExpDirExistsExecption.message.format(self.dir)


def add_directory_to_tar(base_path, current_dir, tar):
    """
    Recursively add a directory and its contents to the tar archive with relative paths.
    
    :param base_path: The base path from which to calculate relative paths
    :param current_dir: The current directory being processed
    :param tar: The tarfile object where files and directories will be added
    """
    print("RECURSE FOR "+current_dir)
    # Convert the current directory to a relative path
    relative_dir = relpath(current_dir, start=base_path)
    
    # Only add the directory if it's not the base directory itself
    if relative_dir != '.':
        print(f"Adding directory: {relative_dir}")  # Debugging output
        tar.add(current_dir, arcname=relative_dir)  # Add the directory with relative path
    
    # Add all files in the current directory
    for root, dirs, files in walk(current_dir):
        for file in files:
            full_path = join(root, file)
            relative_path = relpath(full_path, start=base_path)
            print(f"Adding file: {relative_path}")  # Debugging output
            if relative_path != '.':
                tar.add(full_path, arcname=relative_path)  # Add the file with relative path
            
        # Recursively add subdirectories (if any) by calling the function again
        for dir in dirs:
            if dir == '.' or dir == '..':
                continue
            subdir_path = join(root, dir)
            add_directory_to_tar(base_path, subdir_path, tar) 

def get_relative_path(base_dir, path):
    path = pathlib.Path(path)
    try:
        parts = list(path.parts)
        index = parts.index(base_dir.split(sep)[-1])
        return str(pathlib.Path(*parts[index + 1:]))
    except ValueError:
        return str(path)

def create_exp_dir(name, overwrite=False):
    root_dir = environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set")
    dir = join(root_dir, "tmp", name)
    
    if exists(dir):
        if not overwrite:
            raise ExpDirExistsExecption(dir)
        shutil.rmtree(dir)
    submission_dir = join(dir, 'submissions')
    topology_dir = join(dir, 'topology_files')
    output_dir = join(dir, 'output')
    runs_dir = join(dir, 'runs')

    makedirs(name)
    makedirs(submission_dir)
    makedirs(topology_dir)
    makedirs(output_dir)
    makedirs(runs_dir)

    return  get_relative_path(root_dir, dir),\
            get_relative_path(root_dir, submission_dir),\
            get_relative_path(root_dir, topology_dir),\
            get_relative_path(root_dir, output_dir),\
            get_relative_path(root_dir, runs_dir)   

def store_run_data(exp_name, run_name):
    root_dir = environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set")
    data = exp_meta_data_get(exp_name) 
    run = data['runs'][run_name]
    # Move contents from base dirs to run dirs
    shutil.copytree(join(root_dir, data["submission_dir"]), run['submission_dir'], dirs_exist_ok=True)
    shutil.copytree(join(root_dir, data["topology_dir"]), run['topology_dir'], dirs_exist_ok=True)
    shutil.copytree(join(root_dir, data["output_dir"]), run['output_dir'], dirs_exist_ok=True)
    
    # clean topology dir and ouput_dir
    for _dir in [data['topology_dir'], data['output_dir']]:
        dir = join(root_dir, _dir)
        for item in listdir(dir):
            item_path = join(dir, item)
            if isfile(item_path) or islink(item_path):
                remove(item_path)
            elif isdir(item_path):
                shutil.rmtree(item_path)
    

def create_run_dir(exp_name, run_name, overwrite=False):
    root_dir = environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set")
    data = exp_meta_data_get(exp_name)
    dir = join(root_dir, data["runs_dir"], run_name)
    
    if exists(dir):
        if not overwrite:
            raise ExpDirExistsExecption(dir)
        shutil.rmtree(dir)
    submission_dir = join(dir, 'submissions')
    topology_dir = join(dir, 'topology_files')
    output_dir = join(dir, 'output')
    plot_dir = join(dir, 'plots')

    makedirs(dir)
    makedirs(submission_dir)
    makedirs(topology_dir)
    makedirs(output_dir)
    makedirs(plot_dir)

    return  get_relative_path(root_dir, dir),\
            get_relative_path(root_dir, submission_dir),\
            get_relative_path(root_dir, topology_dir),\
            get_relative_path(root_dir, output_dir),\
            get_relative_path(root_dir, plot_dir), 

class ExpMetaDataStatus:
    STATUS_NONE = "STATUS_NONE"
    # GENERATION
    STATUS_GENERATION_STARTED = "STATUS_GENERATION_STARTED"
    STATUS_GENERATION_COMPLETED = "STATUS_GENERATION_COMPLETED"
    
    # RUN
    STATUS_RUN_STARTED = "STATUS_RUN_STARTED"
    STATUS_RUN_ENDED_WITH_SUCCESS = "STATUS_RUN_ENDED_WITH_SUCCESS"
    STATUS_RUN_COMPLETED = "STATUS_RUN_COMPLETED"
    
    # ERRORS
    STATUS_GENERATION_FAILED = "STATUS_GENERATION_FAILED"
    STATUS_FAILED_TO_START = "STATUS_RUN_FAILED_TO_START"
    STATUS_RUN_ENDED_WITH_ERROR = "STATUS_RUN_ENDED_WITH_ERROR"
    STATUS_SUB_RUN_ENDED_WITH_ERROR = "STATUS_SUB_RUN_ENDED_WITH_ERROR"
    STATUS_RUN_FAILED = "STATUS_RUN_FAILED"


def exp_meta_data_create(name, num_nodes, cores_per_node, policy, policy_params, type='default', timeout=3600, plots=[], plot_params=[], overwrite=False, sub_collection=None, sub_names=[]):

    filename = exp_meta_data_filename(name)
    dir, submission_dir, topology_dir, output_dir, runs_dir = create_exp_dir(dirname(filename), overwrite=overwrite)
    with open(filename, 'w') as outfile:
        yaml.dump({
            'name' : name,
            'type' : type,
            'dir' : dir,
            'submission_dir' : submission_dir,
            'topology_dir' : topology_dir,
            'output_dir' : output_dir,
            'runs_dir' : runs_dir,
            'num_nodes' : num_nodes,
            'cores_per_node' : cores_per_node,
            'timeout' : timeout,
            'supported_plots' : plots,
            'plot_params'   : plot_params,
            'executables' : [],
            'batch_scripts' : [],
            'mix_scripts' : [],
            'status' : ExpMetaDataStatus.STATUS_GENERATION_STARTED,
            'policy' : policy,
            'policy_params' : policy_params,
            'runs' : {},
            'plots' : {},
            'sub_collection': sub_collection,
            'sub_names' : sub_names
        }, 
        outfile, 
        default_flow_style=False)
    
    return dir, submission_dir, topology_dir, output_dir 

def exp_meta_data_create_run(exp_name, run_name):
    run_dir, submission_dir, topology_dir, output_dir, plot_dir = create_run_dir(exp_name, run_name)
    exp_meta_data_insert(exp_name, "runs", [run_name], [{
        'name' : run_name,
        'start_time' : 0,
        'end_time' : 0,
        'status' : ExpMetaDataStatus.STATUS_RUN_STARTED,
        'error' : "None",
        'dir' : run_dir,
        'output_dir' : output_dir,
        'submission_dir' : submission_dir,
        'plot_dir' : plot_dir,
        'topology_dir' : topology_dir     
    }])
    return run_dir, submission_dir, topology_dir, output_dir, plot_dir

def exp_meta_data_set_dirs(name, submission_dir, topology_dir, output_dir, runs_dir):
    data = None
    filename = exp_meta_data_filename(name)
    with open(exp_meta_data_filename(filename), 'r') as file:
        data = yaml.safe_load(file)
    
    data['submission_dir'] = submission_dir
    data['topology_dir'] = topology_dir
    data['output_dir'] = output_dir
    data['plot_dir'] = runs_dir
    
    with open(exp_meta_data_filename(filename), 'w') as file:
        yaml.dump(data, file, default_flow_style=False)
    
def exp_meta_data_set_scale(name, num_nodes, cores_per_node):
    data = None
    filename = exp_meta_data_filename(name)
    with open(filename, 'r') as file:
        data = yaml.safe_load(file)
    data['num_nodes'] = num_nodes
    data['cores_per_node'] = cores_per_node
    with open(filename, 'w') as file:
        yaml.dump(data, file, default_flow_style=False)    

def exp_meta_data_insert(name, key, keys, vals):
    data = None
    filename = exp_meta_data_filename(name)
    with open(filename, 'r') as file:
        data = yaml.safe_load(file)
    for _key,_val in zip(keys, vals):
        data[key][_key] = _val
    
    with open(filename, 'w') as file:
        yaml.dump(data, file, default_flow_style=False)    

def exp_meta_data_append(name, key, vals):
    data = None
    filename = exp_meta_data_filename(name)
    with open(filename, 'r') as file:
        data = yaml.safe_load(file)
    data[key].extend(vals)
    
    with open(filename, 'w') as file:
        yaml.dump(data, file, default_flow_style=False)    

def exp_meta_data_set(name, keys, vals, verbosity=0):
    
    data = None
    filename = exp_meta_data_filename(name)
    with open(filename, 'r') as file:
        data = yaml.safe_load(file)
    
    for key,val in zip(keys,vals):
        data[key]=val
    
    with open(filename, 'w') as file:
        yaml.dump(data, file, default_flow_style=False)

def exp_meta_data_run_set(exp_name, run_name, keys, vals, verbosity=0):
    data = None
    filename = exp_meta_data_filename(exp_name)
    with open(filename, 'r') as file:
        data = yaml.safe_load(file)
    
    for key,val in zip(keys,vals):
        data['runs'][run_name][key]=val
    
    with open(filename, 'w') as file:
        yaml.dump(data, file, default_flow_style=False)    

def exp_meta_data_get(name, keys = None):
    data = None
    filename = exp_meta_data_filename(name)
    with open(filename, 'r') as file:
        data = yaml.safe_load(file)
    
    if None == keys:
        return data

    return {key: data[key] for key in keys}

def exp_meta_data_run_get(exp_name, run_name, keys = None):
    data = None
    filename = exp_meta_data_filename(exp_name)
    with open(filename, 'r') as file:
        data = yaml.safe_load(file)
    
    run = data['runs'][run_name]
    if None == keys:
        return run

    return {key: run[key] for key in keys}

def exp_meta_data_complete(name):
    data = None
    filename = exp_meta_data_filename(name)
    with open(filename, 'r') as file:
        data = yaml.safe_load(file)
    
    data['status'] = ExpMetaDataStatus.STATUS_GENERATION_COMPLETED

    with open(filename, 'w') as file:
        yaml.dump(data, file, default_flow_style=False)

def exp_meta_data_print(name):
    data = None
    filename = exp_meta_data_filename(name)
    with open(filename, 'r') as file:
        data = yaml.safe_load(file)
    
    print("=========METADATA==========")
    print(data)
    print("===========================")


def exp_meta_data_filename(name):
    root_dir = environ.get("BENCHMARKS_ROOT", None)
    if None == root_dir:
        raise Exception("ERROR: 'BENCHMARKS_ROOT' environment variable is not set")
    return join(root_dir, "tmp", name, name+'.yaml')