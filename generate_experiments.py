import experiments.example.example
from utils.metadata.metadata_generation import exp_meta_data_print,exp_meta_data_get
import experiments

import inspect
from os import environ,remove,makedirs
from os.path import join as join,dirname,exists
import tarfile
import shutil
import argparse


def find_functions_with_argument_default(module, argument, target_value):
    results = []
    filter = None
    if target_value == 'all':
        filter = lambda x: True
    elif target_value.startswith('*') and not target_value.endswith('*'):
        filter = lambda x: x.endswith(target_value[1:])
    elif target_value.endswith('*') and not target_value.startswith('*'):
        filter = lambda x: x.startswith(target_value[:-1])
    elif target_value.startswith('*') and target_value.endswith('*'):
        filter = lambda x: x.contains(target_value[1:-1])
    else:
        filter = lambda x: x == target_value

    for name, obj in inspect.getmembers(module, inspect.isfunction):
        if not str(obj.__module__).startswith('experiments.'):
            continue
        sig = inspect.signature(obj)
        if argument in sig.parameters:
            param = sig.parameters[argument]
            if filter(param.default):
                results.append((obj, param.default))
    return results

def generate_experiments(collection, name, num_nodes, cores_per_node, ram_per_node, overwrite, verbosity):

    funcs = find_functions_with_argument_default(eval('experiments.'+collection+'.'+collection), 'name', name)

    if len(funcs) == 0:
        print("Error: No experiments with name '"+name+"' found. Exit!")
        exit(1)
    
    def clean_kwargs(**kwargs):
        return {k: v for k, v in kwargs.items() if v is not None}
    
    for func in funcs:
        print("\nGenerating Experiment '"+func[1]+"' with function '"+func[0].__name__+"'")
        base_name = func[0](**clean_kwargs(name=func[1], 
                                 num_nodes=num_nodes, 
                                 cores_per_node=cores_per_node,
                                 ram_per_node=ram_per_node,
                                 overwrite=overwrite,
                                 verbosity=verbosity))
        if None == base_name:
            print("An error occured while generating the experiment '"+func[1]+"'. Exit!")
            exit(1)

        if exp_meta_data_get(base_name, ['type'])['type'] == 'compound':
            print("Experiment is of type 'compound'. Generating sub experiments:")
            sub_collection = exp_meta_data_get(base_name, ['sub_collection'])['sub_collection']
            sub_names = exp_meta_data_get(base_name, ['sub_names'])['sub_names']
            for sub_name in sub_names:
                print("Generating sub experiment '"+sub_collection+"'=>'"+sub_name+"'")
                generate_experiments(sub_collection, sub_name, num_nodes, cores_per_node, ram_per_node,
                                     overwrite, verbosity)

        print("==> Experiment was successfully generated.")
        if verbosity > 0:
            exp_meta_data_print(base_name)

        dst = join(dirname(__file__), 'archive', collection, base_name+'.tar.gz')
        if(exists(dst)):
            if not overwrite:
                print("ERROR: Experiment dir '"+base_name+".tar.gz' already exists in archive")
                print("Run with --overwrite to overwrite existing dir")
                print("EXIT!")
                exit(1)
            else:
                remove(dst)

        makedirs(dirname(dst), exist_ok=True)
        with tarfile.open(dst, "w:gz") as tar:
            tar.add(join('tmp', base_name), arcname=base_name)

        shutil.rmtree(join(dirname(__file__), 'tmp', base_name))

        print("==> Experiment dir '"+base_name+".tar.gz' added to archive")
        print("*************************************************************")
        print("Run experiment with:")
        print("python3 run_experiments.py "+base_name+" --target <target>")
    
    print("\n Successfully generated "+ str(len(funcs))+" experiment(s) :-)")    

if __name__ == "__main__":
    environ["BENCHMARKS_ROOT"] = dirname(__file__)

    parser = argparse.ArgumentParser(
                    prog='generate_experiments.py',
                    description='Generates a directory containing the experiment',
                    epilog='')
    parser.add_argument('collection', help='The name of the experiment collection to generate from')
    parser.add_argument('name', help='The name of the experiment to generate')
    parser.add_argument('-n', '--num_nodes', type=int, default=8, help='The number of nodes the experiment is executed on')
    parser.add_argument('-c', '--cores_per_node', type=int, default=8, help='The number of cores per node')
    parser.add_argument('-r', '--ram_per_node', type=int, default=1, help='The size of RAM per node (GB)')
    parser.add_argument('-o', '--overwrite', action='store_true', help='Wether existing experiment should be overwritten')
    parser.add_argument('-v', '--verbosity', type=int, default=0, help='Verbosity Level (0-3)')

    args = parser.parse_args()

    generate_experiments(args.collection, args.name, args.num_nodes, args.cores_per_node,
                         args.ram_per_node, args.overwrite, args.verbosity)
    
    

    