from os.path import dirname,join
from os import environ,makedirs
import execo
import argparse
import tarfile
import shutil
import warnings
from datetime import datetime
import time

from utils.metadata.metadata_generation import *
from utils.topologies.generate_topology import *

warnings.filterwarnings("ignore", message="The default behavior of tarfile extraction has been changed to disallow common exploits")


def run_exp_docker_cluster(exp_name, run_name, verbosity=11):
    environ["BENCHMARKS_ROOT"] = dirname(__file__)
    data = exp_meta_data_get(exp_name)
    generate_topology_docker(join(dirname(__file__), data['topology_dir'], data['name']+"_topo.yaml"), int(data['num_nodes']), int(data['cores_per_node']))

    if len(data['mix_scripts']) > 0:
        submission = data['mix_scripts'][0]
    elif len(data['batch_scripts']) > 0:
        submission = data['batch_scripts'][0]
    else:
        return ExpMetaDataStatus.STATUS_FAILED_TO_START, "No submission found for experiment '"+exp_name+"'" 

    command = [ 'python3',
                join(dirname(__file__), "utils", "run", "run_dyn_rm.py"),
                '--topology_file '+join(dirname(__file__), data['topology_dir'], data['name']+"_topo.yaml"),
                '--submission_file '+join(dirname(__file__), submission),
                '--output_dir '+join(dirname(__file__), data['output_dir'], 'dyn_rm_output'),
                '--verbosity '+str(verbosity),
                '--policy '+data['policy'],
                '--policy_params "'+str(data['policy_params'])+'"'
               ]
    process = execo.Process(cmd = " ".join(command), 
                            name = exp_name, 
                            timeout = int(data['timeout']),
                            nolog_timeout=True,
                            nolog_error=True,
                            nolog_exit_code = True,
                            )
    process.stdout_handlers.append(join(dirname(__file__),data['output_dir'], exp_name+".stdout"))
    process.stderr_handlers.append(join(dirname(__file__),data['output_dir'], exp_name+".stderr"))

    exp_meta_data_run_set(exp_name, run_name, ['start_time'], [str(datetime.now()).replace(" ", "_")])
    process.run()
    exp_meta_data_run_set(exp_name, run_name, ['end_time'], [str(datetime.now()).replace(" ", "_")])
    #with open(join(dirname(__file__),data['output_dir'], exp_name+".stdout"), "w") as file:
    #    file.write(process.stdout)
    #with open(join(dirname(__file__),data['output_dir'], exp_name+".stderr"), "w") as file:
    #    file.write(process.stderr)

    if process.timeouted:
        status = ExpMetaDataStatus.STATUS_RUN_ENDED_WITH_ERROR
        error = "Reached timeout of "+str(data['timeout'])+" seconds"
    elif process.exit_code != 0:
        status = ExpMetaDataStatus.STATUS_RUN_ENDED_WITH_ERROR
        error = "Non-zero exit code: "+str(process.exit_code)
    else:
        status = ExpMetaDataStatus.STATUS_RUN_ENDED_WITH_SUCCESS
        error = ""
    exp_meta_data_run_set(exp_name, run_name, ["status", "error"], [status, error])
    return status, error


def run_experiment(collection, exp_name, target, overwrite, verbosity):
    

    collection_dir = join(dirname(__file__), "archive", collection)
    if not os.path.exists(collection_dir):
        print("ERROR: Couldn't find collection dir '"+collection+"' in archive")
        print("EXIT!")
        exit(-1)        

    names = [exp_name]
    filter = None
    if exp_name == 'all':
        filter = lambda x: True
    elif exp_name.startswith('*') and not exp_name.endswith('*'):
        filter = lambda x: x.endswith(exp_name[1:]+".tar.gz")
    elif exp_name.endswith('*') and not exp_name.startswith('*'):
        filter = lambda x: x.startswith(exp_name[:-1])
    elif exp_name.startswith('*') and exp_name.endswith('*'):
        filter = lambda x: x.contains(exp_name[1:-1])
    else:
        filter = lambda x: x == exp_name+'.tar.gz'

    return_status = ExpMetaDataStatus.STATUS_RUN_STARTED
    error=""
    names = sorted([f.replace('.tar.gz', '') for f in listdir(collection_dir) if isfile(join(collection_dir, f)) and filter(f)])
    for name in names:
        # Prepare the run directory
        run_name = name+"_"+target+"_"+str(datetime.now()).replace(" ", "_")
        if verbosity > 0:
            print("==> Starting Experiment: '"+name+"' from the '"+collection+"' collection -> Run: '"+run_name+"'")
        if target != 'nxc-g5k-remote':
            exp_file_name = join(dirname(__file__), "archive", collection, name+".tar.gz")
            if not os.path.exists(exp_file_name):
                print("ERROR: Couldn't find experiment file '"+name+".tar.gz' in archive")
                print("EXIT!")
                exit(-1)
            file = tarfile.open(exp_file_name) 
            file.extractall(join(dirname(__file__), "tmp")) 
            file.close() 
            run_dir, _, _, out_dir, _ = exp_meta_data_create_run(name, run_name)
        
        # RUN THE EXPERIMENT ON TARGET
        start_time = time.time()
        if target == 'bare-metal':
            # Execute the run
            if exp_meta_data_get(name, ['type'])['type'] == 'compound':
                print("Experiment is of type 'compound'. Running sub experiments:")
                sub_collection = exp_meta_data_get(name, ['sub_collection'])['sub_collection']
                sub_names = exp_meta_data_get(name, ['sub_names'])['sub_names']
                num_nodes = str(exp_meta_data_get(name, ['num_nodes'])['num_nodes'])
                cores = str(exp_meta_data_get(name, ['cores_per_node'])['cores_per_node'])

                status = ExpMetaDataStatus.STATUS_RUN_STARTED
                error = ""
                for sub_name in sub_names:
                    print("Running sub experiment '"+sub_collection+"'=>'"+sub_name+"'")
                    sub_status, sub_error = run_experiment(sub_collection, sub_name+'_n'+num_nodes+'_c'+cores, target, overwrite, verbosity)
                    if sub_status != ExpMetaDataStatus.STATUS_RUN_COMPLETED or sub_error != "":
                        status = ExpMetaDataStatus.STATUS_RUN_ENDED_WITH_ERROR
                        error = sub_error
                if status == ExpMetaDataStatus.STATUS_RUN_STARTED:
                   status = ExpMetaDataStatus.STATUS_RUN_ENDED_WITH_SUCCESS 

            else:
                with changedir(join(dirname(__file__), out_dir)):
                    status, error = run_exp_docker_cluster(name, run_name)
        else:
            error = "ERROR: Target '"+target+"' is not supported."
            print(error)
            data = exp_meta_data_get(name, ["runs"])
            data['runs'][run_name]['status'] = ExpMetaDataStatus.STATUS_RUN_FAILED
            data['runs'][run_name]['error'] = error
            exp_meta_data_set(name, ["runs"], [data['runs']])
            return ExpMetaDataStatus.STATUS_RUN_FAILED, error
        runtime = time.time() - start_time

        
        # Copy everything into the run dir and clean base dir
        store_run_data(name, run_name)

        
        # Update run status
        data = exp_meta_data_get(name, ["runs"])
        if status != ExpMetaDataStatus.STATUS_RUN_ENDED_WITH_SUCCESS:
            data['runs'][run_name]['status'] = ExpMetaDataStatus.STATUS_RUN_FAILED
            data['runs'][run_name]['error'] = status +": "+ error
            return_status = ExpMetaDataStatus.STATUS_RUN_FAILED
        else:
            data['runs'][run_name]['error'] = ""
            return_status = ExpMetaDataStatus.STATUS_RUN_ENDED_WITH_SUCCESS
        # This does not mean that it succeeded, but we were able to complete all steps
        data['runs'][run_name]['status'] = ExpMetaDataStatus.STATUS_RUN_COMPLETED
        exp_meta_data_set(name, ["runs"], [data['runs']])


        # Update the archive file
        dst = join(dirname(__file__), 'archive', collection, name+'.tar.gz')
        with tarfile.open(dst, "w:gz") as tar:
            tar.add(join(dirname(__file__), 'tmp', name), arcname=name)
        shutil.rmtree(join(dirname(__file__), 'tmp', name))

        if verbosity > 0:
            print("==> Experiment: '"+name+"' from collection '"+collection+"' -> Run: '"+run_name+"' completed:")
            print("     Status: "+status)
            print("     Error: "+error)
            print("     Runtime: "+str(runtime))
        
    return return_status, error


if __name__ == "__main__":

    environ["BENCHMARKS_ROOT"] = dirname(__file__)

    parser = argparse.ArgumentParser(
                    prog='run_experiments.py',
                    description='Runs an experiment from the archive',
                    epilog='')
    parser.add_argument('collection', help="The collection containing the experiment to run (parent dir of the tarball)")
    parser.add_argument('name', help="The name of the experiment to run (the name of the tarball without '.tar.gz')")
    parser.add_argument('-t', '--target', required=True, help='The target to run the experiment on (docker, nxc-docker, nxc-g5k)')
    parser.add_argument('-o', '--overwrite', action='store_true', help='Wether existing experiment run should be overwritten')
    parser.add_argument('-v', '--verbosity', type=int, default=0, help='Verbosity Level (0-3)')
    args = parser.parse_args()
    
    run_experiment(args.collection, args.name, args.target, args.overwrite, args.verbosity)
